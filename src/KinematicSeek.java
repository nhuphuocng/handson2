public class KinematicSeek {
    private double maxSpeed ;
    private Kinematic character;
    private Kinematic target;

    public KinematicSeek(double maxSpeed, Kinematic character, Kinematic target) {
        this.maxSpeed = maxSpeed;
        this.character = character;
        this.target = target;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public Kinematic getCharacter() {
        return character;
    }

    public void setCharacter(Kinematic character) {
        this.character = character;
    }

    public Kinematic getTarget() {
        return target;
    }

    public void setTarget(Kinematic target) {
        this.target = target;
    }

    @Override
    public String toString() {
        return "KinematicSeek{" + "charater=" + character + ", target=" + target + ", maxSpeed=" + maxSpeed + '}';
    }public KinematicOutput generateKinematicOutput() {
        Vector2D velocity = new Vector2D();
        velocity = character.getPosition().subVector2D(target.getPosition());
        velocity.normalize();
        velocity.multiply(maxSpeed);
        return new KinematicOutput(velocity, 0);
    }
}
