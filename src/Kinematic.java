import java.util.Objects;

public class Kinematic {
    private Vector2D position;
    private Vector2D velocity;
    private double orientation;
    private double rotation;

    public Kinematic(Vector2D position, Vector2D velocity, double orientation, double rotation) {
        this.position = position;
        this.velocity = velocity;
        this.orientation = orientation;
        this.rotation = rotation;
    }

    public Vector2D getPosition() {
        return position;
    }

    public void setPosition(Vector2D position) {
        this.position = position;
    }

    public Vector2D getVelocity() {
        return velocity;
    }

    public void setVelocity(Vector2D velocity) {
        this.velocity = velocity;
    }

    public double getOrientation() {
        return orientation;
    }

    public void setOrientation(double orientation) {
        this.orientation = orientation;
    }

    public double getRotation() {
        return rotation;
    }

    public void setRotation(double rotation) {
        this.rotation = rotation;
    }


    public void update(KinematicOutput kinematicOutput, float time) {
        this.velocity = kinematicOutput.getVelocity();
        this.rotation = kinematicOutput.getRotation();

        this.position.add(this.velocity.multiply(time));
        this.orientation += this.rotation * time;
    }

    public void applyNewOrientation() {
        if (this.velocity.length() > 0) {
            this.orientation = (float) Math.atan2(-this.velocity.getX(), this.rotation);
        }
    }

    @Override
    public String toString() {
        return "Character{" + "position=" + position + ", orientation=" + orientation + ", velocity=" + velocity
                + ", rotation=" + rotation + '}';
    }

}
