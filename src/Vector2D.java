public class Vector2D {
    private double x;
    private double z;
    private double velocity;
    private double position;

    public Vector2D(double x, double z) {
        this.x = x;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public double getVelocity() {
        return velocity;
    }

    public void setVelocity(double velocity) {
        this.velocity = velocity;
    }

    public Vector2D(Vector2D v) {
        set(v);
    }
    public void set(Vector2D v) {
        this.x = v.x;
        this.z = v.z;
    }



    @Override
    public String toString(){
        return "Vector2d[" + x + ", " + z + "]";
    }

    public void add(Vector2D v) {
        this.x += v.x;
        this.z += v.z;
    }

    public void add(float vx, float vz) {
        this.x += vx;
        this.z += vz;
    }

    public static Vector2D add(Vector2D v1, Vector2D v2) {
        return new Vector2D(v1.x + v2.x, v1.z + v2.z);
    }

    public Vector2D getAdded(Vector2D v) {
        return new Vector2D(this.x + v.x, this.z + v.z);
    }
    public void multiply(double scalar) {
        x *= scalar;
        z *= scalar;
    }

    public Vector2D getMultiplied(double scalar) {
        return new Vector2D(x * scalar, z * scalar);
    }
    public void divide(double scalar) {
        x /= scalar;
        z /= scalar;
    }

    public Vector2D getDivided(double scalar) {
        return new Vector2D(x / scalar, z / scalar);
    }


    public double getLength(){
        return Math.sqrt(x * x + z * z);
    }

    public double distance(double vx, double vz) {
        vx -= x;
        vz -= z;
        return Math.sqrt(vx * vx + vz * vz);
    }

    public double distance(Vector2D v) {
        double vx = v.x - this.x;
        double vz = v.z - this.z;
        return Math.sqrt(vx * vx + vz * vz);
    }

    public double getAngle() {
        return Math.atan2(z, x);
    }
    public Vector2D normalize() {
        double length = this.length();
        this.x /= length;
        this.z /= length;
        return this;
    }
    public double length() {
        return Math.sqrt(this.x * this.x + this.z * this.z);
    }

    public Vector2D subVector2D(Vector2D v1, Vector2D v2) {
        return new Vector2D(v1.getX() - v2.getX(), v1.getZ() - v2.getZ());
    }

    public Vector2D subVector2D(Vector2D v1) {
        this.setX(this.getX() - v1.getX());
        this.setZ(this.getZ() - v1.getZ());
        return this;
    }


}
